//called in templates/product/index.php

function productVersionSelected(productVariant) {
    $("#product-price").text(productVariant.Price);
    $("#product-image").attr('src', productVariant.Image);
    $("form#add-to-cart input[name='VariantId']").prop("disabled", false).val(productVariant.Id);
}

ProductConstructor.onDimensionsSelected = productVersionSelected;