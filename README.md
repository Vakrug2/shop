
# Simple interntet shop

Creted for fun, education and may be to show my skills. Slightly inspired by internet shop in "SIA Datatenks".




## Deployment

First, run sql script in sql folder. This will create shop database without data.  
Check app/settings.php file to see database configuration.  
In order to populate database with test data (including admin user) run this console script from project directory:

```bash
  php bin/console.php populate
```

You can change database population settings in file src/Console/PopulateDBWithTestDataCommand.  
After databse is populated you can log in with administrator account `admin@admin.com` with password `123`.  
Every other user also has `123` as a password.