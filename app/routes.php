<?php
declare(strict_types=1);

use App\Application\Controllers\Admin\AdminController;
use App\Application\Controllers\Admin\DimensionController;
use App\Application\Controllers\Admin\DimensionValueController;
use App\Application\Controllers\Admin\OrderController as AdminOrdertController;
use App\Application\Controllers\Admin\ProductController as AdminProductController;
use App\Application\Controllers\Admin\ProductVariantController;
use App\Application\Controllers\CartController;
use App\Application\Controllers\IndexController;
use App\Application\Controllers\OrderController;
use App\Application\Controllers\ProductController;
use App\Application\Controllers\UserController;
use App\Application\Middleware\AdminMiddleware;
use App\Application\Middleware\UserMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', [IndexController::class, 'index'])->setName('home');
    
    $app->group('/products', function (Group $group) {
        $group->get('', [ProductController::class, 'list'])->setName('products');
        $group->get('/{productId}', [ProductController::class, 'index'])->setName('product');
    });
    
    $app->group('/user', function (Group $group) {
        $group->map(['GET', 'POST'], '/login', [UserController::class, 'login'])->setName('login');
        $group->get('/logout', [UserController::class, 'logout'])->setName('logout');
        $group->map(['GET', 'POST'], '/registration', [UserController::class, 'registration'])->setName('registration');
        $group->get('/activation-email-emulation/{userId}', [UserController::class, 'activationEmailEmulation'])->setName('activation-email-emulation');
        $group->get('/activation', [UserController::class, 'activation'])->setName('activation');
    });
    
    $app->group('/admin', function (Group $group) {
        $group->get('', [AdminController::class, 'index'])->setName('admin');
        $group->group('/products', function (Group $group) {
            $group->get('', [AdminProductController::class, 'list'])->setName('admin-products');
            $group->map(['GET', 'POST'], '/edit/{productId}', [AdminProductController::class, 'edit'])->setName('admin-products-edit');
            $group->map(['GET', 'POST'], '/new', [AdminProductController::class, 'new'])->setName('admin-products-new');
            $group->post('/delete/{productId}', [AdminProductController::class, 'delete'])->setName('admin-products-delete');
            $group->group('/{productId}/variants', function (Group $group) {
                $group->get('/list', [ProductVariantController::class, 'list'])->setName('admin-products-varaints');
                $group->map(['GET', 'POST'], '/edit/{variantId}', [ProductVariantController::class, 'edit'])->setName('admin-products-varaints-edit');
                $group->map(['GET', 'POST'], '/create', [ProductVariantController::class, 'create'])->setName('admin-products-varaints-create');
                $group->post('/delete', [ProductVariantController::class, 'delete'])->setName('admin-products-varaints-delete');
            });
        });
        $group->group('/orders', function (Group $group) {
            $group->get('', [AdminOrdertController::class, 'list'])->setName('admin-orders');
            $group->get('/{orderId}', [AdminOrdertController::class, 'view'])->setName('admin-order-view');
        });
        $group->group('/dimensions', function (Group $group) {
            $group->get('', [DimensionController::class, 'list'])->setName('admin-dimensions');
            $group->map(['GET', 'POST'], '/new', [DimensionController::class, 'new'])->setName('admin-dimensions-new');
            $group->map(['GET', 'POST'], '/edit/{dimensionId}', [DimensionController::class, 'edit'])->setName('admin-dimensions-edit');
            $group->post('/delete/{dimensionId}', [DimensionController::class, 'delete'])->setName('admin-dimensions-delete');
            
            $group->group('/{dimensionId}/values', function (Group $group) {
                $group->map(['GET', 'POST'], '/new', [DimensionValueController::class, 'new'])->setName('admin-dimensions-values-new');
                $group->map(['GET', 'POST'], '/edit/{dimensionValueId}', [DimensionValueController::class, 'edit'])->setName('admin-dimensions-values-edit');
                $group->post('/delete/{dimensionValueId}', [DimensionValueController::class, 'delete'])->setName('admin-dimensions-values-delete');
            });
        });        
    })->add(AdminMiddleware::class);
    
    $app->group('/cart', function (Group $group) {
        $group->post('', [CartController::class, 'add'])->setName('add-to-cart');
        $group->get('/clear', [CartController::class, 'clear'])->setName('clear-cart');
        $group->post('/remove-item', [CartController::class, 'removeItem'])->setName('cart-item-remove');
        $group->post('/create-order', [CartController::class, 'createOrder'])->setName('cart-create-order')->add(UserMiddleware::class);
    });
    
    $app->group('/orders', function (Group $group) {
        $group->get('', [OrderController::class, 'list'])->setName('orders');
        $group->get('/{orderId}', [OrderController::class, 'view'])->setName('order-view');
    })->add(UserMiddleware::class);
};
