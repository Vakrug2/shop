<?php
declare(strict_types=1);

use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Application\Settings\SettingsInterface;
use App\Domain\Cart\Cart;
use App\Domain\User;
use DI\ContainerBuilder;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Flash\Messages;
use Slim\Interfaces\RouteParserInterface;
use SlimSession\Helper;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        //Slim\App::class => $app //set in index.php page
        //SettingsInterface set in settings.php
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        PhpRenderer::class => function (ContainerInterface $container) {
            $settings = $container->get(SettingsInterface::class);
            $renderer = new PhpRenderer($settings->get('view')['path']);
            $renderer->setAttributes([
                'container' => $container,
                RouteParserInterface::class => $container->get(RouteParserInterface::class),
                ImageHelper::class => $container->get(ImageHelper::class),
                'currency' => $container->get('currency')
            ]);
            return $renderer;
        },
        'Illuminate.Translation.ArrayLoader' => function() {
            return new ArrayLoader();
        },
        'Illuminate.Translation.Translator' => function(ContainerInterface $container) {
            return new Translator($container->get('Illuminate.Translation.ArrayLoader'), 'en');
        },
        SessionInterface::class => function () {
            return new Helper();
        },
        User::class => function(ContainerInterface $container) {
            $session = $container->get(SessionInterface::class);
            return User::where('Id', $session->get(User::SESSION_AUTHENTICATED_USER_ID))->first();
        },
        RouteParserInterface::class => function(ContainerInterface $container) {
            /** @var App $app */
            $app = $container->get(App::class);
            $routeParser = $app->getRouteCollector()->getRouteParser();
            return $routeParser;
        },
        'flash' => function () {
            $storage = [];
            return new Messages($storage);
        },
        Cart::class => function(ContainerInterface $container) {
            /** @var Helper $session */
            $session = $container->get(SessionInterface::class);
            if ($session->exists('cart')) {
                return Cart::deserialize($session->get('cart'));
            }
            return Cart::deserialize([]);
        },
        'currency' => function(ContainerInterface $container) {
            $settings = $container->get(SettingsInterface::class);
            return $settings->get('currecny');
        },
        ConsoleApplication::class => function (ContainerInterface $container) {
            $application = new ConsoleApplication();
            
            /** @var SettingsInterface $settings */
            $settings = $container->get(SettingsInterface::class);
            $commands = $settings->get('commands');

            foreach ($commands as $class) {
                $application->add($container->get($class));
            }

            return $application;
        },
    ]);
};
