<?php
declare(strict_types=1);

use Slim\App;
use Slim\Middleware\Session;

return function (App $app) {
    $app->add(new Session([
        'lifetime' => 'now' //means until browser is closed //1 hour
    ]));
    
    //flash messages
    $app->add(
        function ($request, $next) use ($app) {
            // Start PHP session
            if (session_status() !== PHP_SESSION_ACTIVE) {
                session_start();
            }

            // Change flash message storage
            $this->get('flash')->__construct($_SESSION);

            return $next->handle($request);
        }
    );
};
