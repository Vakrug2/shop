<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use App\Console\ExampleCommand;
use App\Console\PopulateDBWithTestDataCommand;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'view' => [
                    'path' => __DIR__ . '/../src/templates',
                ],
                'db' => [
                    'driver' => 'mysql',
                    'host' => 'localhost',
                    'database' => 'shop',
                    'username' => 'root',
                    'password' => '',
                    'charset' => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix' => ''
                ],
                'product' => [
                    'image' => [ //default product images (paths) are not stored in DB, but applys to this convension
                        'folder' => __DIR__ . '/../public/images/products/',
                        'path' => '/images/products/',
                        'prefix' => 'product',
                        'extension' => 'jpg',
                        'variant' => [ //images (paths) for product's different varinats are stored in DB
                            'prefix' => 'variant',
                            'folder' => __DIR__ . '/../public/images/products/variants/', //for storing only
                            'path' => '/images/products/variants/'
                        ]
                    ]
                ],
                'currecny' => [
                    'code' => 'USD',
                    'symbol' => '$'
                ],
                'commands' => [
                    ExampleCommand::class,
                    PopulateDBWithTestDataCommand::class
                ]
            ]);
        }
    ]);
};
