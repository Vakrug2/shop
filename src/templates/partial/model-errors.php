<?php

use App\Domain\Model;

/** @var Model $model */
?>

<?php if ($model->errors->any()): ?>
<ul class="list-group">
    <?php foreach ($model->errors->all() as $error): ?>
    <li class="list-group-item list-group-item-danger"><?= $error ?></li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>