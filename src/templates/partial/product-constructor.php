<?php

use App\Application\ProductConstructor\ProductConstructor;
use App\Application\Renderer\PhpRenderer;

/** @var PhpRenderer $this */
/** @var ProductConstructor $productConstructor */

$this->addJavascriptInline('product-constructor', $productConstructor->getJavascript());
?>

<?php foreach ($productConstructor->getProduct()->productDimensions as $productDimension): ?>
<div id="<?= $productConstructor->htmlElementId ?>">
    <?= $productDimension->dimension->Name ?>:
    <?php foreach ($productDimension->dimension->dimensionValues as $dimensionValue): ?>
    <label>
        <?= $dimensionValue->Value ?>
        <input type="radio" name="<?= $productDimension->DimensionId ?>" value="<?= $dimensionValue->Id ?>" />
    </label>
    <?php endforeach; ?>
</div>
<?php endforeach; ?>

<hr />