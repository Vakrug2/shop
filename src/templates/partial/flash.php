<?php

use App\Application\Renderer\PhpRenderer;
use DI\Container;
use Slim\Flash\Messages;

/** @var PhpRenderer $this */

/** @var Container $container */
$container = $this->getAttribute('container');
/** @var Messages $flash */
$flash = $container->get('flash');

if (count($flash->getMessages()) == 0) {
    return;
}
?>

<ul class="list-group">
    <?php foreach ($flash->getMessages() as $flashMessageArray): ?>
        <?php foreach ($flashMessageArray as $flashMessage): ?>
    <li class="list-group-item list-group-item-danger">
            <?= $flashMessage ?>
    </li>
        <?php endforeach; ?>
    <?php endforeach; ?>
</ul>