<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Cart\Cart;
use App\Domain\Cart\CartItem;
use App\Domain\User;
use DI\Container;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */
/** @var Container $container */
$container = $this->getAttribute('container');
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
/** @var Cart $cart */
$cart = $container->get(Cart::class);
/** @var User $authenticatedUser */
$authenticatedUser = $container->get(User::class);
$currencySymbol = $this->getAttribute('currency')['symbol'];
?>

<table class="table table-striped">
    <tbody>
        <?php foreach ($cart->getContent() as $itemArray): ?>
            <?php foreach ($itemArray as $item): ?>
            <?php /** @var CartItem $item */ ?>
                <tr>
                    <td><?= $item->Name ?></td>
                    <td><?= $item->Price . $currencySymbol ?></td>
                    <td><?= $item->Quantity ?></td>
                    <td><?= $item->VariantText ?></td>
                    <td>
                        <form method="POST" action="<?= $routeParser->urlFor('cart-item-remove') ?>">
                            <input type="hidden" name="Id" value="<?= $item->Id ?>" />
                            <input type="hidden" name="VariantId" value="<?= $item->VariantId ?>" />
                            <input class="btn btn-secondary" type="submit" value="X" />
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                Sum: <?= $cart->sum() . $currencySymbol ?>
            </td>
        </tr>
    </tfoot>
</table>
<p><a href="<?= $routeParser->urlFor('clear-cart') ?>">Clear</a></p>
<p>
    <?php if (!$cart->isEmpty()): ?>
        <?php if ($authenticatedUser): ?>
        <form method="POST" action="<?= $routeParser->urlFor('cart-create-order') ?>">
            <input class="btn btn-secondary" type="submit" value="Buy" />
        </form>
    <?php else: ?>
        Login to buy
    <?php endif; ?>
<?php endif; ?>
</p>