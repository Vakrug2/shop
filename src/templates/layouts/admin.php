<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\User;
use DI\Container;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */

/** @var Container $container */
$container = $this->getAttribute('container');
/** @var User $authenticatedUser */
$authenticatedUser = $container->get(User::class);
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="/styles/common.css" rel="stylesheet" type="text/css"/>
    <script
        src="https://code.jquery.com/jquery-3.6.1.js"
        integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
        crossorigin="anonymous">
    </script>
</head>
<body>
    <div class="container">
        <div class="row page-header">
            <nav class="navbar navbar-expand">
                <div class="container-fluid">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $routeParser->urlFor('home') ?>">Home</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <?php if ($authenticatedUser): ?>
                        <li class="nav-item">
                            <p class="navbar-text">User: <?= $authenticatedUser->Email ?></p>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $routeParser->urlFor('logout') ?>">Logout</a>
                        </li>
                        <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $routeParser->urlFor('login') ?>">Login</a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="row">
            <div class="col-3 sidebar">
                <a href="<?= $routeParser->urlFor('admin') ?>">Admin Home</a><br />
                <a href="<?= $routeParser->urlFor('admin-products') ?>">Products</a><br />
                <a href="<?= $routeParser->urlFor('admin-orders') ?>">Orders</a><br />
                <a href="<?= $routeParser->urlFor('admin-dimensions') ?>">Dimensions</a><br />
            </div>
            <div class="col-9">
                <?= $this->fetch('partial/flash.php') ?>
                <?= $content ?>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <?php $this->renderJavascript() ?>
</body>
</html>