<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Order;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */
/** @var Order[] $orders */
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);

?>
<?php if (empty($orders)): ?>
<p>No orders</p>
    <?php return; ?>
<?php endif; ?>

<?php foreach ($orders as $order): ?>
<p>
    <a href="<?= $routeParser->urlFor('order-view', ['orderId' => $order->Id]) ?>"><?= $order->DateTime ?></a>
</p>
<?php endforeach; ?>

