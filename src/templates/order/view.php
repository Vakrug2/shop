<?php
//simmilar to admin/order/view
use App\Application\Renderer\PhpRenderer;
use App\Domain\Order;

/** @var PhpRenderer $this */
/** @var Order $order */
$currencySymbol = $this->getAttribute('currency')['symbol'];
?>
<h1>
    <?= $order->DateTime ?>
</h1>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Variant</th>
            <th>Price</th>
            <th>Qty</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($order->orderProducts as $orderProduct): ?>
        <tr>
            <td><?= $orderProduct->product->Name ?></td>
            <td><?= $orderProduct->productVariant ? $orderProduct->productVariant->getVariantText() : "" ?></td>
            <td><?= $orderProduct->Price . $currencySymbol ?></td>
            <td><?= $orderProduct->Qty ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4">
                Total: <?= $order->total() . $currencySymbol ?>
            </td>
        </tr>
    </tfoot>
</table>