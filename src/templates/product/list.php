<?php

use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */
/** @var Product[] $products */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
/** @var ImageHelper $imageHelper */
$imageHelper = $this->getAttribute(ImageHelper::class);
$currencySymbol = $this->getAttribute('currency')['symbol'];
?>
<div class="container-fluid">
    <div class="row">
        <?php foreach ($products as $product): ?>
        <div class="col mb-3 mt-3">
            <div class="card" style="width: 18rem;">
                <img src="<?= $imageHelper->productImageSrc($product) ?>" class="card-img-top" />
                <div class="card-body">
                    <h5 class="card-title">
                        <a href="<?= $routeParser->urlFor('product', ['productId' => $product->Id]) ?>"><?= $product->Name ?></a>
                    </h5>
                    <p class="card-text">
                        <?= $product->Price . $currencySymbol ?>
                        <?php if (count($product->productDimensions) > 0): ?>
                        <span>(has variants)</span>
                        <?php endif; ?>
                    </p>
                    <form method="post" action="<?= $routeParser->urlFor('add-to-cart') ?>">
                        <input type="hidden" name="Id" value="<?= $product->Id ?>" />
                        <div class="input-group">
                            <input type="number" name="Count" value="1" min="1" step="1" class="form-control" />
                            <button class="btn btn-primary" type="submit">Add to cart</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>