<?php

use App\Application\Helpers\ImageHelper;
use App\Application\ProductConstructor\ProductConstructor;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */
/** @var Product $product */
/** @var ProductConstructor $productConstructor */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);

/** @var ImageHelper $imageHelper */
$imageHelper = $this->getAttribute(ImageHelper::class);
$currencySymbol = $this->getAttribute('currency')['symbol'];
?>
<h1><?= $product->Name ?></h1>
<div class="container">
    <div class="row">
        <div class="col">
            <img id="product-image" src="<?= $imageHelper->productImageSrc($product) ?>" />
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p><span id="product-price"><?= $product->Price ?></span><span><?= $currencySymbol ?></span></p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $this->fetch('partial/product-constructor.php', ['productConstructor' => $productConstructor]) ?>
            <?php $this->addJavascriptFile('product-constructor-subscription', '/scripts/product-page-product-constructor-subscription.js') ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form id="add-to-cart" method="post" action="<?= $routeParser->urlFor('add-to-cart') ?>">
                <input type="hidden" name="Id" value="<?= $product->Id ?>" />
                <input type="hidden" name="VariantId" value="0" disabled="disabled" />
                <div class="input-group">
                    <input type="number" name="Count" value="1" min="1" step="1" class="form-control" />
                    <button class="btn btn-primary" type="submit">Add to cart</button>
                </div>
            </form>
        </div>
    </div>
</div>