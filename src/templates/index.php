<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\User;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
/** @var User $authenticatedUser */
$authenticatedUser = $container->get(User::class);
?>
<nav class="navbar">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?= $routeParser->urlFor('products') ?>">Products</a>
        </li>
        <?php if ($authenticatedUser): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= $routeParser->urlFor('orders') ?>">Orders</a>
        </li>
        <?php endif; ?>
    </ul>
</nav>