<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var Product $product */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>

<?= $this->fetch('admin/product/_form.php', ['product' => $product])?>
<a href='<?= $routeParser->urlFor('admin-products') ?>'>Back</a>