<?php

use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
/** @var PhpRenderer $this */
/** @var Product $product */
/** @var ImageHelper $imageHelper */
$imageHelper = $this->getAttribute(ImageHelper::class);
?>
<?= $this->fetch('partial/model-errors.php', ['model' => $product])?>
<form method='POST' enctype="multipart/form-data">
    <div>
        <label for="product-name" class="form-label">Name</label>
        <input type="text" name='Name' value='<?= $product->Name ?>' class="form-control" id="product-name" />
    </div>
    <div>
        <label for="product-price" class="form-label">Price</label>
        <input type="number" step="0.01" name='Price' value='<?= $product->Price ?>' class="form-control" id="product-price" />
    </div>
    <div>
        <label for="product-image" class="form-label">Image</label>
        <input type="file" accept='<?= $imageHelper->productAccept() ?>' name='<?= ImageHelper::UPLOAD_INPUT_NAME ?>' class="form-control-file" id="product-image" />
    </div>
    <input type='submit' class="btn btn-primary" value='Save' />
</form>