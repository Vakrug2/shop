<?php

use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var Product $product */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
/** @var ImageHelper $imageHelper */
$imageHelper = $this->getAttribute(ImageHelper::class);
?>

<p>
    <img src='<?= $imageHelper->productImageSrc($product) ?>' />
</p>
<p>
    <?= $product->dimensionsText() ?>
    <a href='<?= $routeParser->urlFor('admin-products-varaints', ['productId' => $product->Id]) ?>'>Variants</a>
</p>

<?= $this->fetch('admin/product/_form.php', ['product' => $product])?>
<a href='<?= $routeParser->urlFor('admin-products') ?>'>Back</a>