<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;

/** @var Product[] $products */
/** @var PhpRenderer $this */
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
$currencySymbol = $this->getAttribute('currency')['symbol'];
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Dimensions<th>
            <th>Price</th>
            <th />
            <th />
        </tr>
    </thead>
    <tbody>
<?php foreach ($products as $product): ?>
        <tr>
            <td><?= $product->Name ?></td>
            <td><?= $product->dimensionsText() ?></td>
            <td><?= $product->Price . $currencySymbol ?></td>
            <td>
                <a href="<?= $routeParser->urlFor('admin-products-edit', ['productId' => $product->Id]) ?>">Edit</a>
            </td>
            <td>
                <form method="POST" action="<?= $routeParser->urlFor('admin-products-delete', ['productId' => $product->Id]) ?>">
                    <button class="btn btn-secondary" type="submit">Delete</button>
                </form>
            </td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
<a href="<?= $routeParser->urlFor('admin-products-new') ?>">New</a>