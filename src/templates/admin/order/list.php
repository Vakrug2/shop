<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Order;
use Slim\Interfaces\RouteParserInterface;

/** @var Order[] $orders */
/** @var PhpRenderer $this */
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>
<?php if (empty($orders)): ?>
<p>No orders</p>
    <?php return; ?>
<?php endif; ?>
<?php foreach ($orders as $order): ?>
<p>
    <?= $order->user->Email ?>:
    <a href="<?= $routeParser->urlFor('admin-order-view', ['orderId' => $order->Id]) ?>"><?= $order->DateTime ?></a>
</p>
<?php endforeach; ?>
