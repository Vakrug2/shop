<?php

use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */
/** @var Product $product */
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
/** @var ImageHelper $imageHelper */
$imageHelper = $this->getAttribute(ImageHelper::class);
?>
<h1>Variants of <?= $product->Name ?></h1>
<p>
    <img src='<?= $imageHelper->productImageSrc($product) ?>' />
</p>
<?php
$dimensionCount = count($product->productDimensions);

$productVariantDimensionValuesArray = [];
foreach ($product->productVariantDimensionValues as $productVariantDimensionValue) {
    $productVariantDimensionValuesArray[$productVariantDimensionValue->ProductVariantId][$productVariantDimensionValue->dimensionValue->DimensionId] = $productVariantDimensionValue;
}
?>
<?php if ($dimensionCount > 0): ?>
<table>
    <tr>
        <?php foreach ($product->productDimensions as $productDimension): ?>
        <th>
            <?= $productDimension->dimension->Name ?>
        </th>
        <?php endforeach; ?>
        <th>Price</th>
        <th>Image</th>
        <th></th>
    </tr>
    <?php foreach($productVariantDimensionValuesArray as $productVariantId => $productVariantDimensionValuesElement): ?>
    <tr>
        <?php foreach ($product->productDimensions as $productDimension): ?>
        <td>
            <?= $productVariantDimensionValuesElement[$productDimension->DimensionId]->dimensionValue->Value ?>
        </td>
        <?php endforeach; ?>
        <td><?= $productVariantDimensionValuesElement[array_key_first($productVariantDimensionValuesElement)]->productVariant->Price;  ?></td>
        <td>
            <img src="<?= $productVariantDimensionValuesElement[array_key_first($productVariantDimensionValuesElement)]->productVariant->Image;  ?>" width="30px" height="30px" />
        </td>
        <td><a href="<?= $routeParser->urlFor('admin-products-varaints-edit', ['productId' => $product->Id, 'variantId' => $productVariantId]) ?>">Edit</a></td>
    </tr>
    <?php endforeach; ?>
</table>
<p>
    <form method='POST' action="<?= $routeParser->urlFor('admin-products-varaints-delete', ['productId' => $product->Id]) ?>">
        <input type="submit" class="btn btn-danger" value="Delete all variants" />
    </form>
</p>
<?php else: ?>
<p><a href="<?= $routeParser->urlFor('admin-products-varaints-create', ['productId' => $product->Id]) ?>">Create variants</a></p>
<?php endif; ?>

<a href="<?= $routeParser->urlFor('admin-products-edit', ['productId' => $product->Id]) ?>">Back</a>