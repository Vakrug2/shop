<?php

use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use App\Domain\ProductVariant;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var Product $product */
/** @var ProductVariant $productVariant */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
/** @var ImageHelper $imageHelper */
$imageHelper = $this->getAttribute(ImageHelper::class);
?>
<h1>
    Edit variant for product <?= $product->Name ?> 
</h1>
<h2>
    <?= $productVariant->getVariantText() ?>
</h2>

<p>
    <img src='<?= $productVariant->Image ?>' />
</p>

<?= $this->fetch('partial/model-errors.php', ['model' => $productVariant])?>
<form method='POST' enctype="multipart/form-data">
    <div>
        <label for="product-variant-price" class="form-label">Price</label>
        <input type="number" step="0.01" name='Price' value='<?= $productVariant->Price ?>' class="form-control" id="product-variant-price" />
    </div>
    <div>
        <label for="product-variant-image" class="form-label">Image</label>
        <input type="file" accept='<?= $imageHelper->productAccept() ?>' name='<?= ImageHelper::UPLOAD_INPUT_NAME ?>' class="form-control-file" id="product-variant-image" />
    </div>
    <input type='submit' class="btn btn-primary" value='Save' />
</form>

<a href='<?= $routeParser->urlFor('admin-products-varaints', ['productId' => $product->Id]) ?>'>Back</a>