<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
use App\Domain\Product;
use Slim\Interfaces\RouteParserInterface;

/** @var PhpRenderer $this */
/** @var Product $product */
/** @var Dimension[] $dimensions */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>
<h1>Choose dimensions for product <?= $product->Name ?></h1>
<form method="POST">
    <?php foreach($dimensions as $dimension): ?>
    <label>
        <?= $dimension->Name ?>
        <input type="checkbox" name="dimensions[]" value="<?= $dimension->Id ?>" />
    </label>
    <br />
    <?php endforeach; ?>
    <input type="submit" class="btn btn-primary" value="Create">
</form>
<a href='<?= $routeParser->urlFor('admin-products-varaints', ['productId' => $product->Id]) ?>'>Back</a>