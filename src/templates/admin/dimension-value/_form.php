<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\DimensionValue;
/** @var PhpRenderer $this */
/** @var DimensionValue $dimensionValue */
?>
<?= $this->fetch('partial/model-errors.php', ['model' => $dimensionValue]) ?>
<form method='POST'>
    <div>
        <label for="product-name" class="form-label">Value</label>
        <input type="text" name='Value' value='<?= $dimensionValue->Value ?>' class="form-control" id="dimension-value-value" />
    </div>
    <input type='submit' class="btn btn-primary" value='Save' />
</form>