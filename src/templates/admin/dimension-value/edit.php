<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\DimensionValue;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var DimensionValue $dimensionValue */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>

<h1>Edit dimension value for <?= $dimensionValue->dimension->Name ?></h1>

<?= $this->fetch('admin/dimension-value/_form.php', ['dimensionValue' => $dimensionValue])?>
<a href='<?= $routeParser->urlFor('admin-dimensions-edit', ['dimensionId' => $dimensionValue->DimensionId]) ?>'>Back</a>