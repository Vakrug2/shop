<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
/** @var PhpRenderer $this */
/** @var Dimension $dimension */
?>
<?= $this->fetch('partial/model-errors.php', ['model' => $dimension]) ?>
<form method='POST'>
    <div>
        <label for="product-name" class="form-label">Name</label>
        <input type="text" name='Name' value='<?= $dimension->Name ?>' class="form-control" id="dimension-name" />
    </div>
    <input type='submit' class="btn btn-primary" value='Save' />
</form>