<h1>Edit dimension</h1>
<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var Dimension $dimension */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>

<?= $this->fetch('admin/dimension/_form.php', ['dimension' => $dimension])?>

<h2>Dimension values</h2>
<table>
<?php foreach($dimension->dimensionValues as $dimensionValue): ?>
    <tr>
        <td><?= $dimensionValue->Value ?></td>
        <td><a href='<?= $routeParser->urlFor('admin-dimensions-values-edit', [
            'dimensionId' => $dimension->Id,
            'dimensionValueId' => $dimensionValue->Id
        ]) ?>'>Edit</a><br /></td>
        <td>
            <form method="POST" action="<?= $routeParser->urlFor('admin-dimensions-values-delete', [
                'dimensionId' => $dimension->Id,
                'dimensionValueId' => $dimensionValue->Id,
            ]) ?>">
                <button class="btn btn-secondary" type="submit">Delete</button>
            </form>
        </td>
    </tr>
<?php endforeach; ?>
</table>
<a href='<?= $routeParser->urlFor('admin-dimensions-values-new', ['dimensionId' => $dimension->Id]) ?>'>Add new value</a><br />

<a href='<?= $routeParser->urlFor('admin-dimensions') ?>'>Back</a>