<?php

use App\Domain\Dimension;
use Slim\Interfaces\RouteParserInterface;
use Slim\Views\PhpRenderer;

/** @var PhpRenderer $this */
/** @var Dimension[] $dimensions */
/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>

<h1>Dimension list</h1>

<table>
    <?php foreach ($dimensions as $dimension): ?>
    <tr>
        <td><?= $dimension->Name ?></td>
        <td><a href="<?= $routeParser->urlFor('admin-dimensions-edit', ['dimensionId' => $dimension->Id]) ?>">Edit</a></td>
        <td>
            <form method="POST" action="<?= $routeParser->urlFor('admin-dimensions-delete', ['dimensionId' => $dimension->Id]) ?>">
                <button class="btn btn-secondary" type="submit">Delete</button>
            </form>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<a href="<?= $routeParser->urlFor('admin-dimensions-new') ?>">Create new dimension</a>