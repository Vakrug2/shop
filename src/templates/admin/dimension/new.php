<h1>New dimension</h1>
<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
use Slim\Interfaces\RouteParserInterface;
/** @var PhpRenderer $this */
/** @var Dimension $dimension */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);
?>
<?= $this->fetch('admin/dimension/_form.php', ['dimension' => $dimension])?>
<a href='<?= $routeParser->urlFor('admin-dimensions') ?>'>Back</a>