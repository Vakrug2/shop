<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\User;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteParserInterface;

/** @var User $user */
/** @var PhpRenderer $this */
/** @var ServerRequestInterface $request */

/** @var RouteParserInterface $routeParser */
$routeParser = $this->getAttribute(RouteParserInterface::class);

$activationUrl = $routeParser->fullUrlFor($request->getUri(), 'activation', [], ['Email' => $user->Email, 'ActivationHash' => $user->ActivationHash]);
?>
<a href="<?= $activationUrl ?>">Activation</a>