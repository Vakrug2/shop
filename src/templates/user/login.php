<?php

use App\Application\Renderer\PhpRenderer;
use App\Domain\User;
    
/** @var User $user */
/** @var PhpRenderer $this */
?>
<?= $this->fetch('partial/model-errors.php', ['model' => $user])?>

<form method="POST">
    <div>
        <label for="user-email" class="form-label">Email</label>
        <input type="email" name='Email' value='<?= $user->Email ?>' class="form-control" id="user-email" />
    </div>
    <div>
        <label for="user-password" class="form-label">Password</label>
        <input type="password" name='Password' class="form-control" id="user-password" />
    </div>
    <input class="btn btn-primary" type="submit" value="Submit" />
</form>
