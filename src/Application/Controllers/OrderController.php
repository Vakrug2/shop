<?php

namespace App\Application\Controllers;

use App\Application\Renderer\PhpRenderer;
use App\Domain\Order;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class OrderController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/main.php');
    }
    
    public function view(ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $order = Order::query()->where([
            'Id' => $args['orderId'],
            'UserId' => $this->user->Id
        ])->with(['orderProducts', 'orderProducts.product',
            'orderProducts.productVariant', 'orderProducts.productVariant.productVariantDimensionValues', 'orderProducts.productVariant.productVariantDimensionValues.dimensionValue'
        ])->first();
        if (!$order) {
            return $this->redirect($response, $this->routeParser->urlFor('home'));
        }
        
        return $this->renderer->render($response, 'order/view.php', ['order' => $order]);
    }
    
    public function list(ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $orders = Order::query()->where(['UserId' => $this->user->Id])->orderBy('DateTime', 'desc')->get();
        return $this->renderer->render($response, 'order/list.php', ['orders' => $orders]);
    }
}
