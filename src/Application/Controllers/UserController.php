<?php

namespace App\Application\Controllers;

use App\Application\Renderer\PhpRenderer;
use App\Domain\User;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/main.php');
    }
    
    public function login(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $userLoginForm = (new User())->forLogin();
        
        if ($request->getMethod() == 'POST') {
            $userLoginForm->fill($request->getParsedBody());
            if ($userLoginForm->validate()) {
                /** @var User $user */
                $user = User::query()->where('Email', $userLoginForm->Email)->first();
                if ($user) {
                    $success = $user->login($userLoginForm->Password);
                    if ($success) {
                        return $this->redirect($response, $this->routeParser->urlFor('home'));
                    } else {
                        $userLoginForm->errors->add('login-failed', 'Login failed');
                    }
                }
            }
        }
        
        return $this->renderer->render($response, 'user/login.php', ['user' => $userLoginForm]);
    }
    
    public function logout(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $user = new User();
        $user->logout();
        return $this->redirect($response, $this->routeParser->urlFor('home'));
    }
    
    public function registration(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $userRegistrationForm = (new User())->forRegistration();
        
        if ($request->getMethod() == 'POST') {
            $userRegistrationForm->fill($request->getParsedBody());
            if ($userRegistrationForm->validate()) {
                if ($userRegistrationForm->register()) {
                    //emulate sending mail with activation link
                    return $this->redirect($response, $this->routeParser->urlFor('activation-email-emulation', ['userId' => $userRegistrationForm->Id]));
                } else {
                    $userRegistrationForm->errors->add('registration-failed', 'Registration failed');
                }
            }
        }
        
        return $this->renderer->render($response, 'user/registration.php', ['user' => $userRegistrationForm]);
    }
    
    public function activationEmailEmulation(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $user = User::query()->where('Id', $args['userId'])->first();
        return $this->renderer->render($response, 'user/activation-email-emulation.php', [
            'user' => $user,
            'request' => $request
        ]);
    }
    
    public function activation(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $params = $request->getQueryParams();
        /** @var User $user */
        $user = User::query()->where('Email', $params['Email'])->where('ActivationHash', $params['ActivationHash'])->first();
        if ($user) {
            $user->IsActive = 1;
            $user->ActivationHash = null;
            $user->save();
            $user->loginWithoutPassword();
            return $this->redirect($response, $this->routeParser->urlFor('home'));
        }
        
        return $response->getBody()->write('User activation failed');
    }
}
