<?php

namespace App\Application\Controllers;

use App\Application\Renderer\PhpRenderer;
use App\Domain\Cart\Cart;
use App\Domain\User;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Flash\Messages;
use Slim\Interfaces\RouteParserInterface;
use SlimSession\Helper;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

abstract class BaseController {
    /** @var ContainerInterface */
    protected $container;
    
    /** @var PhpRenderer */
    protected $renderer;
    
    /** @var RouteParserInterface */
    protected $routeParser;
    
    /** @var Messages */
    protected $flash;
    
    /** @var Cart */
    protected $cart;
    
    /** @var Helper */
    protected $session;
    
    /** @var User */
    protected $user;

    // constructor receives container instance
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        $this->container = $container;
        $this->renderer = $renderer;
        $this->routeParser = $this->container->get(RouteParserInterface::class);
        $this->flash = $this->container->get('flash');
        $this->cart = $this->container->get(Cart::class);
        $this->session = $this->container->get(SessionInterface::class);
        $this->user = $this->container->get(User::class);
    }
    
    public function redirect(ResponseInterface $response, $to) {
        return $response->withHeader('Location', $to)->withStatus(302);
    }
}
