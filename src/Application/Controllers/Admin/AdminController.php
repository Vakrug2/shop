<?php

namespace App\Application\Controllers\Admin;

use App\Application\Controllers\BaseController;
use App\Application\Renderer\PhpRenderer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AdminController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/admin.php');
    }
    
    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {        
        return $this->renderer->render($response, 'admin/index.php');
    }
}