<?php

namespace App\Application\Controllers\Admin;

use App\Application\Controllers\BaseController;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Order;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class OrderController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/admin.php');
    }
    
    public function view(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $order = Order::query()->where([
            'Id' => $args['orderId'],
        ])->with('orderProducts', 'orderProducts.product')->first();
        if (!$order) {
            return $this->redirect($response, $this->routeParser->urlFor('admin'));
        }
        
        return $this->renderer->render($response, 'admin/order/view.php', ['order' => $order]);
    }
    
    public function list(ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $orders = Order::query()->orderBy('DateTime', 'desc')->with('user')->get();
        return $this->renderer->render($response, 'admin/order/list.php', ['orders' => $orders]);
    }
}
