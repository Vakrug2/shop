<?php

namespace App\Application\Controllers\Admin;

use App\Application\Controllers\BaseController;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DimensionController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/admin.php');
    }
    
    public function list(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimensions = Dimension::all();
        return $this->renderer->render($response, 'admin/dimension/list.php', ['dimensions' => $dimensions]);
    }
    
    public function new(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimension = new Dimension();
        
        if ($request->getMethod() == 'POST') {
            $dimension->fill($request->getParsedBody());
            if ($dimension->validate()) {
                $dimension->save();
                return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions'));
            }
        }
        
        return $this->renderer->render($response, 'admin/dimension/new.php', ['dimension' => $dimension]);
    }
    
    public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimensionId = $args['dimensionId'];
        $dimension = Dimension::with(['dimensionValues'])->where('Id', $dimensionId)->first();
        
        if ($request->getMethod() == 'POST') {
            $dimension->fill($request->getParsedBody());
            if ($dimension->validate()) {
                $dimension->save();
                return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions'));
            }
        }
        
        return $this->renderer->render($response, 'admin/dimension/edit.php', ['dimension' => $dimension]);
    }
    
    public function delete(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimensionId = $args['dimensionId'];
        $dimension = Dimension::query()->where('Id', $dimensionId)->first();
        if ($dimension) {
            try {
                $dimension->delete();
                $this->flash->addMessage('dimension-deleted', 'Dimension deleted');
            } catch (QueryException $e) {
                $this->flash->addMessage('dimension-not-deleted', $e->getMessage());
            }
        }
        return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions'));
    }
}
