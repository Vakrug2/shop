<?php

namespace App\Application\Controllers\Admin;

use App\Application\Controllers\BaseController;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
use App\Domain\DimensionValue;
use Illuminate\Database\QueryException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DimensionValueController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/admin.php');
    }
    
    public function new(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimensionId = $args['dimensionId'];
        $dimension = Dimension::query()->find($dimensionId);
        if (!$dimension) {
            $this->flash->addMessage('dimension-not-found', 'Dimension not found: ' . $dimensionId);
            return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions'));
        }
        
        $dimensionValue = new DimensionValue();
        $dimensionValue->dimension()->associate($dimension);
        
        if ($request->getMethod() == 'POST') {
            $dimensionValue->fill($request->getParsedBody());
            if ($dimensionValue->validate()) {
                $dimensionValue->save();
                return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions-edit', ['dimensionId' => $dimensionValue->DimensionId]));
            }
        }
        
        return $this->renderer->render($response, 'admin/dimension-value/new.php', [
            'dimensionValue' => $dimensionValue
        ]);
    }
    
    public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimensionId = $args['dimensionId'];
        $dimensionValueId = $args['dimensionValueId'];
        $dimensionValue = DimensionValue::with('dimension')->where([
            'Id' => $dimensionValueId,
            'DimensionId' => $dimensionId
        ])->first();
        
        if (!$dimensionValue) {
            $this->flash->addMessage('dimension-value-not-found', 'Dimension value not found: ' . $dimensionId . ' - ' . $dimensionValueId);
            return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions'));
        }
        
        if ($request->getMethod() == 'POST') {
            $dimensionValue->fill($request->getParsedBody());
            if ($dimensionValue->validate()) {
                $dimensionValue->save();
                return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions-edit', ['dimensionId' => $dimensionValue->DimensionId]));
            }
        }
        
        return $this->renderer->render($response, 'admin/dimension-value/edit.php', [
            'dimensionValue' => $dimensionValue
        ]);
    }
    
    public function delete(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $dimensionId = $args['dimensionId'];
        $dimensionValueId = $args['dimensionValueId'];
        $dimensionValue = DimensionValue::query()->where([
            'Id' => $dimensionValueId,
            'DimensionId' => $dimensionId
        ])->first();
        if ($dimensionValue) {
            try {
                $dimensionValue->delete();
                $this->flash->addMessage('dimension-value-deleted', 'Dimension value deleted');
            } catch (QueryException $e) {
                $this->flash->addMessage('dimension-value-not-deleted', $e->getMessage());
            }
        }
        return $this->redirect($response, $this->routeParser->urlFor('admin-dimensions-edit', ['dimensionId' => $dimensionValue->DimensionId]));
    }
}
