<?php

namespace App\Application\Controllers\Admin;

use App\Application\Controllers\BaseController;
use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Illuminate\Database\QueryException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ProductController extends BaseController {
    
    /** @var ImageHelper */
    private $imageHelper;
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer, ImageHelper $imageHelper) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/admin.php');
        $this->imageHelper = $imageHelper;
    }
    
    public function list(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        /** @var Product[] $products */
        $products = Product::with('productDimensions', 'productDimensions.dimension')->get();
        
        return $this->renderer->render($response, 'admin/product/list.php', ['products' => $products]);
    }
    
    public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $productId = $args['productId'];
        $product = Product::query()->where('Id', $productId)->first();
        
        if ($request->getMethod() == 'POST') {
            $product->fill($request->getParsedBody());
            if ($product->validate()) {
                $product->save();
                $this->imageHelper->productImageSave($request, $product);
                return $this->redirect($response, $this->routeParser->urlFor('admin-products'));
            }
        }
        
        return $this->renderer->render($response, 'admin/product/edit.php', ['product' => $product]);
    }
    
    public function new(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $product = new Product();
        
        if ($request->getMethod() == 'POST') {
            $product->fill($request->getParsedBody());
            if ($product->validate()) {
                $product->save();
                $this->imageHelper->productImageSave($request, $product);
                return $this->redirect($response, $this->routeParser->urlFor('admin-products'));
            }
        }
        
        return $this->renderer->render($response, 'admin/product/new.php', ['product' => $product]);
    }
    
    public function delete(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $productId = $args['productId'];
        $product = Product::query()->where('Id', $productId)->first();
        if ($product) {
            try {
                $product->delete();
                $this->flash->addMessage('product-deleted', 'Product deleted');
            } catch (QueryException $e) {
                $this->flash->addMessage('product-not-deleted', $e->getMessage());
            }
        }
        return $this->redirect($response, $this->routeParser->urlFor('admin-products'));
    }
}