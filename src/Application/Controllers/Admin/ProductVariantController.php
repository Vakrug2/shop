<?php

namespace App\Application\Controllers\Admin;

use App\Application\Controllers\BaseController;
use App\Application\Helpers\ImageHelper;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Dimension;
use App\Domain\Product;
use App\Domain\ProductVariant;
use Exception;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ProductVariantController extends BaseController {
    
    /** @var ImageHelper */
    private $imageHelper;
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer, ImageHelper $imageHelper) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/admin.php');
        $this->imageHelper = $imageHelper;
    }
    
    public function list(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $productId = $args['productId'];
        $product = Product::with([
            'productDimensions', 'productDimensions.dimension',
            'productVariantDimensionValues', 'productVariantDimensionValues.productVariant', 'productVariantDimensionValues.dimensionValue'
        ])->where('Id', $productId)->first();
        
        return $this->renderer->render($response, 'admin/product-variant/list.php', ['product' => $product]);
    }
    
    public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $productId = $args['productId'];
        $variantId = $args['variantId'];
        
        /** @var Product $product */
        $product = Product::query()->find($productId);
        /** @var ProductVariant $productVariant */
        $productVariant = ProductVariant::with(['productVariantDimensionValues'])->find($variantId);
        if (empty($productVariant->productVariantDimensionValues)) {
            throw new Exception('Product varinat not found for a given product.');
        }
        if ($productVariant->productVariantDimensionValues[0]->ProductId != $product->Id) {
            throw new Exception('Product varinats not found for a given product.');
        }
        
        if ($request->getMethod() == 'POST') {
            $productVariant->fill($request->getParsedBody());
            if ($productVariant->validate()) {
                $this->imageHelper->productVariantImageSave($request, $product, $productVariant);
                $productVariant->save();
                return $this->redirect($response, $this->routeParser->urlFor('admin-products-varaints', ['productId' => $product->Id]));
            }
        }
        
        return $this->renderer->render($response, 'admin/product-variant/edit.php', ['product' => $product, 'productVariant' => $productVariant]);
    }
    
    public function create(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $productId = $args['productId'];
        /** @var Product $product */
        $product = Product::with(['productDimensions'])->find($productId);
        if (count($product->productDimensions) > 0) {
            throw new Exception('Looks like dimensions are already created for this product.');
        }
        
        if ($request->getMethod() == 'POST') {
            if (!isset($request->getParsedBody()['dimensions'])) {
                throw new Exception('Bad request');
            }
            $selectedDimensionIds = $request->getParsedBody()['dimensions'];
            if (!is_array($selectedDimensionIds)) {
                throw new Exception('Bad request');
            }
            
            $dimensions = Dimension::with(['dimensionValues'])->whereIn('Id', $selectedDimensionIds)->get();
            $product->createdProductVariants($dimensions);
            
            return $this->redirect($response, $this->routeParser->urlFor('admin-products-varaints', ['productId' => $product->Id]));
        }
        
        $dimensions = Dimension::all();
        
        return $this->renderer->render($response, 'admin/product-variant/create.php', [
            'product' => $product,
            'dimensions' => $dimensions
        ]);
    }
    
    public function delete(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $productId = $args['productId'];
        /** @var Product $product */
        $product = Product::with(['productDimensions', 'productVariantDimensionValues', 'productVariantDimensionValues.productVariant'])->find($productId);
        
        if ($request->getMethod() == 'POST') {
            $product->deleteAllVariants();
        }
        
        return $this->redirect($response,  $this->routeParser->urlFor('admin-products-varaints', ['productId' => $product->Id]));
    }
}
