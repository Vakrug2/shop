<?php

namespace App\Application\Controllers;

use App\Application\Controllers\BaseController;
use App\Domain\Cart\AddToCartForm;
use App\Domain\Cart\CartItem;
use App\Domain\Order;
use App\Domain\Product;
use App\Domain\User;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CartController extends BaseController {
    
    public function add(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $addToCartForm = new AddToCartForm();
        $addToCartForm->fill($request->getParsedBody());
        if ($addToCartForm->validate()) {
            $with = [];
            if ($addToCartForm->VariantId) {
                $with = ['productVariantDimensionValues', 'productVariantDimensionValues.productVariant'];
            }
            /** @var Product $product */
            $product = Product::with($with)->where('Id', $addToCartForm->Id)->first();
            if ($addToCartForm->VariantId && !$product->checkVariantIdIsLegal($addToCartForm->VariantId)) {
                $product = null;
            }
            if ($product) {
                $cartItem = CartItem::createFromProduct($product, $addToCartForm->Count, $addToCartForm->VariantId);
                $this->cart->addItem($cartItem);
                $this->session->set('cart', $this->cart->serialize());
            } else {
                $this->flash->addMessage('add-to-cart-failed', 'Product not found');
            }
        } else {
            $this->flash->addMessage('add-to-cart-failed', $addToCartForm->errors->first());
        }
        
        return $this->redirect($response, $this->routeParser->urlFor('products'));
    }
    
    public function clear(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $this->cart->clear();
        $this->session->set('cart', $this->cart->serialize());
        
        return $this->redirect($response, $this->routeParser->urlFor('products'));
    }
    
    public function removeItem(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $itemId = $request->getParsedBody()['Id'] ?? null;
        $variantId = $request->getParsedBody()['VariantId'] ?? null;
        if ($variantId == "") {
            $variantId = null;
        }
        if ($variantId) {
            $variantId = (int)$variantId;
        }
        $this->cart->removeItem($itemId, $variantId);
        $this->session->set('cart', $this->cart->serialize());
        
        return $this->redirect($response, $this->routeParser->urlFor('products'));
    }
    
    public function createOrder(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        $orderId = Order::createFromCart($this->cart, $this->user);
        
        $this->cart->clear();
        $this->session->set('cart', $this->cart->serialize());
        
        return $this->redirect($response, $this->routeParser->urlFor('order-view', ['orderId' => $orderId]));
    }
}
