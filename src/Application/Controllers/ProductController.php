<?php

namespace App\Application\Controllers;

use App\Application\Helpers\ImageHelper;
use App\Application\ProductConstructor\ProductConstructor;
use App\Application\Renderer\PhpRenderer;
use App\Domain\Product;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ProductController extends BaseController {
    
    public function __construct(ContainerInterface $container, PhpRenderer $renderer) {
        parent::__construct($container, $renderer);
        $this->renderer->setLayout('layouts/main.php');
    }
    
    public function list(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        /** @var Product[] $products */
        $products = Product::with(['productDimensions'])->get();
        
        return $this->renderer->render($response, 'product/list.php', ['products' => $products]);
    }
    
    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
        /** @var Product $product */
        $product = Product::with([
            'productDimensions', 'productDimensions.dimension', 'productDimensions.dimension.dimensionValues',
            'productVariantDimensionValues', 'productVariantDimensionValues.productVariant',
            'productVariantDimensionValues.dimensionValue'
        ])->where([
            'Id' => $args['productId']
        ])->first();
        
        $productConstructor = new ProductConstructor($product, $this->container->get(ImageHelper::class));
        
        return $this->renderer->render($response, 'product/index.php', [
            'product' => $product,
            'productConstructor' => $productConstructor
        ]);
    }
}