<?php

namespace App\Application\Helpers;

use App\Application\Settings\SettingsInterface;
use App\Domain\Product;
use App\Domain\ProductVariant;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;

class ImageHelper {
    
    const UPLOAD_INPUT_NAME = 'Image';
    
    /** @var ContainerInterface */
    private $container;
    
    /** @var SettingsInterface */
    private $settings;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->settings = $this->container->get(SettingsInterface::class);
    }
    
    private function productImageName(Product $product) {
        $productSetting = $this->settings->get('product');
        return $productSetting['image']['prefix'] . $product->Id . '.' . $productSetting['image']['extension'];
    }
    
    private function productVariantImageName(ProductVariant $productVariant) {
        $productSetting = $this->settings->get('product');
        return $productSetting['image']['variant']['prefix'] . $productVariant->Id . '.' . $productSetting['image']['extension'];
    }
    
    public function productImageSrc(Product $product) {
        $productSetting = $this->settings->get('product');
        return $productSetting['image']['path'] . $this->productImageName($product);
    }
    
    public function productAccept() {
        $productSetting = $this->settings->get('product');
        return '.' . $productSetting['image']['extension'];
    }
    
    public function productImageSave(ServerRequestInterface $request, Product $product) {
        $productSetting = $this->settings->get('product');
        $uploadedFiles = $request->getUploadedFiles();
        /** @var UploadedFileInterface $uploadedFile */
        $uploadedFile = $uploadedFiles[ImageHelper::UPLOAD_INPUT_NAME];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $uploadedFile->moveTo($productSetting['image']['folder'] . $this->productImageName($product));
        }
    }
    
    public function productVariantImageSave(ServerRequestInterface $request, Product $product, ProductVariant $productVariant) {
        $productSetting = $this->settings->get('product');
        $uploadedFiles = $request->getUploadedFiles();
        /** @var UploadedFileInterface $uploadedFile */
        $uploadedFile = $uploadedFiles[ImageHelper::UPLOAD_INPUT_NAME];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            if (!file_exists($productSetting['image']['variant']['folder'] . $product->Id)) {
                mkdir($productSetting['image']['variant']['folder'] . $product->Id, 0777, true);
            }
            $uploadedFile->moveTo($productSetting['image']['variant']['folder'] . $product->Id . '/' . $this->productVariantImageName($productVariant));
            $productVariant->Image = $productSetting['image']['variant']['path'] . $product->Id . '/' . $this->productVariantImageName($productVariant);
        }
    }
}
