<?php

namespace App\Application\Middleware;

use App\Domain\User;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AdminMiddleware extends BaseMiddleware implements MiddlewareInterface {
    
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->container->get(User::class);
        
        if ($authenticatedUser && $authenticatedUser->IsAdmin) {
            return $handler->handle($request);
        }
        
        return $this->redirect($this->routeParser->urlFor('home'));
    }
}