<?php

namespace App\Application\Middleware;

use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Interfaces\RouteParserInterface;

class BaseMiddleware {
    protected $container;
    
    /** @var RouteParserInterface */
    protected $routeParser;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->routeParser = $this->container->get(RouteParserInterface::class);
    }
    
    public function redirect($to) {
        /** @var App $app */
        $app = $this->container->get(App::class);
        $response = $app->getResponseFactory()->createResponse();
        return $response->withHeader('Location', $to)->withStatus(302);
    }
}
