<?php

namespace App\Application\Renderer;

use Exception;
use Slim\Views\PhpRenderer as SlimPhpRenderer;

class PhpRenderer extends SlimPhpRenderer {
    
    /** @var IJavascriptRenderItem[] */
    protected $javascripts = [];
    
    public function renderJavascript() {
        $usedKeys = [];
        
        foreach ($this->javascripts as $key => $javascriptItem) {
            if (in_array($key, $usedKeys)) {
                // Not supposed to be executed since we check keys during adding.
                throw new Exception('Tryed to print 2 javascripts with same key: ' . $key);
            }
            $usedKeys[] = $key;
            $javascriptItem->render();
        }
    }
    
    public function addJavascriptFile($key, string $path) {
        if (array_key_exists($key, $this->javascripts)) {
            throw new Exception('Key already exists: ' . $key);
        }
        $this->javascripts[$key] = new JavascriptRenderFile($path);
    }
    
    public function addJavascriptInline($key, string $script) {
        if (array_key_exists($key, $this->javascripts)) {
            throw new Exception('Key already exists: ' . $key);
        }
        $this->javascripts[$key] = new JavascriptRenderInline($script);
    }
}