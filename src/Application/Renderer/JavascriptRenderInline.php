<?php

namespace App\Application\Renderer;

class JavascriptRenderInline implements IJavascriptRenderItem {
    
    private $script;
    
    public function __construct(string $script) {
        $this->script = $script;
    }
    
    public function render() {
        echo '<script>' . $this->script . '</script>';
    }
}