<?php

namespace App\Application\Renderer;

class JavascriptRenderFile implements IJavascriptRenderItem {
    
    private $path;
    
    public function __construct(string $path) {
        $this->path = $path;
    }
    
    public function render() {
        echo '<script src="' . $this->path . '"></script>';
    }
}