<?php

namespace App\Application\Renderer;

interface IJavascriptRenderItem {
    
    public function render();
}
