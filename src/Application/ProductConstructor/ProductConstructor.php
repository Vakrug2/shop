<?php

namespace App\Application\ProductConstructor;

use App\Application\Helpers\ImageHelper;
use App\Domain\Product;
use Exception;

class ProductConstructor {
    
    /** @var Product */
    private $product;
    
    private $variantsTree = [];
    
    /** @var int[] */
    private $dimensionHierarchy = [];
    
    private $productVariants = [];
    
    /** @var ImageHelper */
    private $imageHelper;
    
    public $htmlElementId = 'product-constructor';
    
    public function __construct(Product $product, ImageHelper $imageHelper) {
        
        $this->product = $product;
        
        $this->imageHelper = $imageHelper;
        
        foreach ($product->productDimensions as $productDimension) {
            $this->dimensionHierarchy[] = $productDimension->DimensionId;
        }
        
        $intermediateTransformedVarinats = [];
        foreach ($product->productVariantDimensionValues as $productVariantDimensionValue) {
            $intermediateTransformedVarinats[$productVariantDimensionValue->ProductVariantId][$productVariantDimensionValue->dimensionValue->DimensionId] = $productVariantDimensionValue->DimensionValueId;
        }
        
        $lastHierachyId = null;
        if (count($this->dimensionHierarchy)) {
            $lastHierachyId = $this->dimensionHierarchy[array_key_last($this->dimensionHierarchy)];
        }
        foreach ($intermediateTransformedVarinats as $variantId => $variant) {            
            $pointer = &$this->variantsTree;
            foreach ($this->dimensionHierarchy as $dimensionId) {
                if (!isset($pointer[$variant[$dimensionId]])) {
                    if ($dimensionId == $lastHierachyId) {
                        $pointer[$variant[$dimensionId]] = $variantId;
                    } else {
                        $pointer[$variant[$dimensionId]] = [];
                        $pointer = &$pointer[$variant[$dimensionId]];
                    }
                } else {
                    if ($dimensionId == $lastHierachyId) {
                        throw new Exception("No way this is the last hierarchy Id!");
                    } else {
                        $pointer = &$pointer[$variant[$dimensionId]];
                    }
                }
            }
        }
        //Now product variant id can be accessed with
        //$productVariantId = $this->variantsTree[dimension1Value][dimension2Value]...[dimensionNValue];
        
        foreach ($product->productVariantDimensionValues as $productVariantDimensionValue) {
            $this->productVariants[$productVariantDimensionValue->ProductVariantId] =
                [
                    'Id' => $productVariantDimensionValue->ProductVariantId,
                    'Price' => $productVariantDimensionValue->productVariant->Price,
                    'Image' => $productVariantDimensionValue->productVariant->Image ?? $imageHelper->productImageSrc($product)
                ];
        }
    }
    
    public function getProduct(): Product {
        return $this->product;
    }
    
    public function getVariantsTree(): array {
        return $this->variantsTree;
    }
    
    public function getDimensionHierarchy(): array {
        return $this->dimensionHierarchy;
    }
    
    public function getProductVariants(): array {
        return $this->productVariants;
    }
    
    public function getJavascript() {
        $variantsTreeJs = json_encode($this->getVariantsTree());
        $dimensionsCount = count($this->getDimensionHierarchy());
        $dimensionHierarchyJs = json_encode($this->getDimensionHierarchy());
        $productVariantsJs = json_encode($this->getProductVariants());
        
        $js = <<<JS
                var ProductConstructor = {};
                ProductConstructor.DimesionsCount = $dimensionsCount;
                ProductConstructor.VariantsTree = $variantsTreeJs;
                ProductConstructor.onDimensionsSelected = function() {};
                ProductConstructor.DimensionHierarchy = $dimensionHierarchyJs;
                ProductConstructor.ProductVariants = $productVariantsJs;
                
                $("#{$this->htmlElementId} input").change(function() {
                    var setDimensions = {};
                    var setDimensionsCount = 0;
                    $("#{$this->htmlElementId} input:checked").each(function() {
                        setDimensions[$(this).attr("name")] = this.value;
                        setDimensionsCount++;
                    });
                    if (ProductConstructor.DimesionsCount == setDimensionsCount) {
                        var productVariant = ProductConstructor.VariantsTree;
                        ProductConstructor.DimensionHierarchy.forEach(function(dimension) {
                            productVariant = productVariant[setDimensions[dimension]];
                        });
                        ProductConstructor.onDimensionsSelected(ProductConstructor.ProductVariants[productVariant]);
                    }
                });
JS;
        return $js;
    }
}