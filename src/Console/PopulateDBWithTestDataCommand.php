<?php

namespace App\Console;

use App\Domain\User;
use Illuminate\Database\Capsule\Manager as DB;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PopulateDBWithTestDataCommand extends Command {
    
    const ORDER_COUNT = 20;
    const USER_COUNT = 5; //No more than 27, no less than 1 (naming generation reasons)
    const PRODUCT_COUNT = 10;
    
    private $productNames = [
        'product', //with id = 0 for not found products
        'Triangle',
        'Square',
        'Cube',
        'Circle',
        'Ball'
    ];
    
    private $dimensions = [
        'color' => [
            'white',
            'red',
            'green',
            'blue',
            'black'
        ],
        'size' => [
            'small',
            'medium',
            'big'
        ],
        'material' => [
            'paper',
            'wood',
            'plastic',
            'metal'
        ],
        'transparency' => [
            'transparent',
            'not transparent'
        ],
        'dimensions' => [
            '2d',
            '3d'
        ]
    ];
    
    private $dimensionValueIds = []; //$this->dimensionValueIds[$dimensionId][] = $dimensionValueId;
    
    private $productDimensionIds = []; //$this->productDimensionIds[$productId][] = $dimensionId;
    
    private $productPrices = []; //$this->productPrices[$productId] = $prodctPrice;
    
    private $productVariants = []; //$this->productVariants[$productId][] = $productVariantId;
    
    private $productVariantPrices = []; //$this->productVariantPrices[$productVariantId] = $variantPrice;
    
    protected function configure(): void {
        parent::configure();

        $this->setName('populate');
        $this->setDescription('Populate database with test data. Database must be already created');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int {
        $productVariantDimensionValueTable = DB::table('product_variant_dimension_value');
        $productVariantDimensionValueTable->delete();
        
        $orderProductTable = DB::table('order_product');
        $orderProductTable->delete();
        DB::statement('ALTER TABLE order_product AUTO_INCREMENT = 1');
        
        $productVariantTable = DB::table('product_variant');
        $productVariantTable->delete();
        DB::statement('ALTER TABLE product_variant AUTO_INCREMENT = 1');
        
        $productDimensionTable = DB::table('product_dimension');
        $productDimensionTable->delete();
        
        $dimensionValueTable = DB::table('dimension_value');
        $dimensionValueTable->delete();
        DB::statement('ALTER TABLE dimension_value AUTO_INCREMENT = 1');
        
        $dimensionTable = DB::table('dimension');
        $dimensionTable->delete();
        DB::statement('ALTER TABLE dimension AUTO_INCREMENT = 1');
        
        $orderTable = DB::table('order');
        $orderTable->delete();
        DB::statement('ALTER TABLE `order` AUTO_INCREMENT = 1');
        
        $productTable = DB::table('product');
        $productTable->delete();
        DB::statement('ALTER TABLE product AUTO_INCREMENT = 1');
        
        $userTable = DB::table('user');
        $userTable->delete();
        DB::statement('ALTER TABLE `user` AUTO_INCREMENT = 1');
        
        //user
        foreach (range(1, PopulateDBWithTestDataCommand::USER_COUNT) as $userId) {
            if ($userId == 1) { //then create our admin
                $userTable->insert([
                    'IsActive' => 1,
                    'IsAdmin' => 1,
                    'Email' => 'admin@admin.com',
                    'PasswordHash' => password_hash('123', PASSWORD_DEFAULT)
                ]);
            } else {
                $alphabet = $this->getAlphabet();
                $userTable->insert([
                    'IsActive' => 1,
                    'IsAdmin' => 0,
                    'Email' => str_repeat($alphabet[$userId - 2], 3) . '@ooo.lv',
                    'PasswordHash' => User::createPasswordHash('123')
                ]);
            }
        }
        
        //product
        $productPrices = [];
        foreach (range(1, PopulateDBWithTestDataCommand::PRODUCT_COUNT) as $productId) {
            $prodctPrice = rand(10, 99);
            $productPrices[$productId] = $prodctPrice;
            $productTable->insert([
                'Id' => $productId,
                'Name' => $this->getProductName($productId),
                'Price' => $prodctPrice
            ]);
            $this->productPrices[$productId] = $prodctPrice;
        }
        
        //dimension / dimension_value
        foreach ($this->dimensions as $dimensionName => $dimensionValues) {
            $dimensionId = $dimensionTable->insertGetId([
                'Name' => $dimensionName
            ]);
            foreach ($dimensionValues as $dimensionValue) {
                $dimensionValueId = $dimensionValueTable->insertGetId([
                    'DimensionId' => $dimensionId,
                    'Value' => $dimensionValue
                ]);
                $this->dimensionValueIds[$dimensionId][] = $dimensionValueId;
            }
        }
        
        //product_dimension
        // 1/3 chance for a product to have dimensions
        foreach (range(1, PopulateDBWithTestDataCommand::PRODUCT_COUNT) as $productId) {
            if (rand(0, 1)) { //50% for a product to have variants
                //between 1 and 3 dimensions
                foreach ($this->getRandomElements(array_keys($this->dimensionValueIds), 3) as $dimensionId) {
                    $productDimensionTable->insert([
                        'ProductId' => $productId,
                        'DimensionId' => $dimensionId
                    ]);
                    $this->productDimensionIds[$productId][] = $dimensionId;
                }
                
                //cartesian product https://stackoverflow.com/questions/6311779/finding-cartesian-product-with-php-associative-arrays
                $cartesianProduct = [[]];
                foreach ($this->productDimensionIds[$productId] as $dimensionId) {
                    $append = [];
                    foreach ($this->dimensionValueIds[$dimensionId] as $dimensionValueId) {
                        foreach ($cartesianProduct as $data) {
                            $append[] = $data + [$dimensionId => $dimensionValueId];
                        }
                    }
                    $cartesianProduct = $append;
                }
                
                //product_variant_dimension_value
                foreach ($cartesianProduct as $dimensionValueIds) {
                    //product_variant
                    $variantPrice = $this->productPrices[$productId] + rand(-10, 10);
                    $productVariantId = $productVariantTable->insertGetId([
                        'Price' => $variantPrice
                    ]);
                    $this->productVariantPrices[$productVariantId] = $variantPrice;
                    foreach ($dimensionValueIds as $dimensionValueId) {
                        $productVariantDimensionValueTable->insert([
                            'ProductVariantId' => $productVariantId,
                            'ProductId' => $productId,
                            'DimensionValueId' => $dimensionValueId
                        ]);
                    }
                    $this->productVariants[$productId][] = $productVariantId;
                }
            }
        }
        
        //order
        $randomDateTimesForOrders = $this->getRandomDateTimesForOrders();
        foreach (range(1, PopulateDBWithTestDataCommand::ORDER_COUNT) as $orderId) {
            $orderTable->insert([
                'UserId' => rand(1, PopulateDBWithTestDataCommand::USER_COUNT),
                'DateTime' => $randomDateTimesForOrders[$orderId - 1]
            ]);
        }
        
        //order_product
        foreach (range(1, PopulateDBWithTestDataCommand::ORDER_COUNT) as $orderId) {
            foreach ($this->getRandomElements(range(1, PopulateDBWithTestDataCommand::PRODUCT_COUNT), rand(2, 4)) as $productId) {
                $insertArray = [
                    'ProductId' => $productId,
                    'OrderId' => $orderId,
                    'Price' => $productPrices[$productId],
                    'Qty' => rand(1, 5)
                ];
                $boughtVariantedProduct = false;
                if (isset($this->productVariants[$productId])) {
                    if (!rand(0, 2)) { //33% chance to buy varianted product
                        $insertArray['ProductVariantId'] = $this->productVariants[$productId][rand(0, count($this->productVariants[$productId]) - 1)];
                        $insertArray['Price'] = $this->productVariantPrices[$insertArray['ProductVariantId']];
                        $boughtVariantedProduct = true;
                    }
                }
                $orderProductTable->insert($insertArray);
                if ($boughtVariantedProduct) {
                    if (!rand(0, 3)) { //25% to buy non varianted product
                        $orderProductTable->insert([
                            'ProductId' => $productId,
                            'OrderId' => $orderId,
                            'Price' => $productPrices[$productId],
                            'Qty' => rand(1, 5)
                        ]);
                    }
                }
            }
        }
        
        $output->writeln('Done');
        
        return 0;
    }
    
    private function getRandomElements(array $source, int $quantity) {
        shuffle($source);
        return array_slice($source, 0, $quantity);
    }
    
    private function getAlphabet() {
        return range('a', 'z');
    }
    
    private function getProductName($productId) {
        if (isset($this->productNames[$productId])) {
            return $this->productNames[$productId];
        }
        return $this->productNames[0] . $productId;
    }
    
    private function getRandomDateTimesForOrders() {
        $from = strtotime('2021-10-01');
        $to = strtotime('2021-11-01');
        $dateTimesUnix = [];
        foreach (range(1, PopulateDBWithTestDataCommand::ORDER_COUNT) as $orderId) {
            $dateTimesUnix[] = rand($from, $to);
        }
        sort($dateTimesUnix);
        $dateTimes = [];
        foreach ($dateTimesUnix as $singleDateTimeUnix) {
            $dateTimes[] = date('Y-m-d H-i-s', $singleDateTimeUnix);
        }
        return $dateTimes;
    }
}
