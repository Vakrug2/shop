<?php

namespace App\Domain;

use App\Domain\Cart\Cart;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * @property int $Id
 * @property int $UserId
 * @property date $DateTime
 * 
 * @property OrderProduct[] $orderProducts
 * @property User $user
 */
class Order extends Model {
    protected $table = 'order';
    
    protected $fillable = [
        'UserId', 'DateTime'
    ];
    
    public function orderProducts() {
        return $this->hasMany(OrderProduct::class, 'OrderId', 'Id');
    }
    
    public function user() {
        return $this->belongsTo(User::class, 'UserId', 'Id');
    }
    
    //Not sure is this is usefull
    //public function products() {
    //    return $this->hasManyThrough(Product::class, OrderProduct::class);
    //}
    
    public static function createFromCart(Cart $cart, User $user) {
        $orderId = 0;
        DB::transaction(function() use ($cart, $user, &$orderId) {
            $order = new Order();
            $order->UserId = $user->Id;
            $order->DateTime = date('Y-m-d H-i-s');
            $order->save();
            $orderId = $order->Id;

            foreach ($cart->getContent() as $cartItem) {
                foreach ($cartItem as $cartItemVariant) {
                    $orderProduct = new OrderProduct();
                    $orderProduct->OrderId = $order->Id;
                    $orderProduct->Price = $cartItemVariant->Price;
                    $orderProduct->ProductId = $cartItemVariant->Id;
                    $orderProduct->Qty = $cartItemVariant->Quantity;
                    $orderProduct->ProductVariantId = $cartItemVariant->VariantId;
                    $orderProduct->save();
                }
            }
        });
        return $orderId;
    }
    
    public function total() {
        $total = 0;
        foreach ($this->orderProducts as $orderProduct) {
            $total += $orderProduct->Price * $orderProduct->Qty;
        }
        return $total;
    }
}
