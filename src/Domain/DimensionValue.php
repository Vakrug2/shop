<?php

namespace App\Domain;

/**
 * @property int $Id
 * @property int $DimensionId
 * @property string $Value
 * 
 * @property Dimension $dimension
 * @property ProductVariantDimensionValue $productVariantDimensionValues
 */
class DimensionValue extends Model {
    protected $table = 'dimension_value';
    
    protected $fillable = [
        'DimensionId', 'Value'
    ];
    
    protected $rules = [
        'DimensionId' => 'required',
        'Value' => 'required'
    ];
    
    public function dimension() {
        return $this->belongsTo(Dimension::class, 'DimensionId', 'Id');
    }
    
    public function productVariantDimensionValues() {
        return $this->hasMany(ProductVariantDimensionValue::class, 'DimensionValueId', 'Id');
    }
}