<?php

namespace App\Domain;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @property int $Id
 * @property bool $IsActive
 * @property bool $IsAdmin
 * @property string $Email
 * @property string $PasswordHash
 * @property string $ActivationHash
 * 
 * @property string $Password
 * @property string $RepeatPassword
 * 
 * @property Order[] $orders
 */
class User extends Model {
    const SESSION_AUTHENTICATED_USER_ID = 'authenticated-user-id';
    
    protected $table = 'user';
    
    protected $fillable = [
        'Email'
    ];
    
    protected $additionalFields = [
        'Password',
        'RepeatPassword'
    ];
    
    protected $rules = [
        'Email' => 'required|email|unique:App\Domain\User,Email',
    ];
    
    protected $validationMessages = [
        'Email.required' => 'Email is required',
        'Email.email' => 'Email is not an email',
        'Email.unique' => 'Email already taken',
        'Password.required' => 'Password missing',
        'RepeatPassword.required' => 'Repeat password missing',
        'RepeatPassword.same' => 'Repeated password not the same as password'
    ];
    
    public function forLogin() {
        $this->fillable = [
            'Email', 'Password'
        ];
        $this->rules = [
            'Email' => 'required|email',
            'Password' => 'required'
        ];
        return $this;
    }
    
    public function forRegistration() {
        $this->fillable = [
            'Email', 'Password', 'RepeatPassword'
        ];
        $this->rules = [
            'Email' => 'required|email|unique:App\Domain\User,Email',
            'Password' => 'required',
            'RepeatPassword' => 'required|same:Password'
        ];
        return $this;
    }
    
    public function login($password) {
        $success = password_verify($password, $this->PasswordHash);
        if ($success) {
            /** @var SessionInterface $session */
            $session = $this->container->get(SessionInterface::class);
            $session->set(User::SESSION_AUTHENTICATED_USER_ID, $this->Id);
        }
        return $success;
    }
    
    public function loginWithoutPassword() {
        $session = $this->container->get(SessionInterface::class);
        $session->set(User::SESSION_AUTHENTICATED_USER_ID, $this->Id);
    }
    
    public function logout() {
        $session = $this->container->get(SessionInterface::class);
        $session->set(User::SESSION_AUTHENTICATED_USER_ID, null);
    }
    
    public function register() {
        $this->IsActive = 0;
        $this->IsAdmin = 0;
        $this->PasswordHash = User::createPasswordHash($this->Password);
        $this->ActivationHash = md5($this->Email . $this->Password);
        return $this->save();
    }
    
    public function orders() {
        return $this->hasMany(Order::class, 'UserId', 'Id');
    }
    
    public static function createPasswordHash($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}
