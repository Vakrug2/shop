<?php

namespace App\Domain\Cart;

class Cart {
    
    /**
     * Structure: [itemId][itemVariant] itemVariant can be null
     */
    private $content = [];
    
    public function getContent() {
        return $this->content;
    }
    
    public function addItem(CartItem $item) {
        if (isset($this->content[$item->Id][$item->VariantId])) {
            $this->content[$item->Id][$item->VariantId]->Quantity += $item->Quantity;
            return;
        }
        $this->content[$item->Id][$item->VariantId] = $item;
    }
    
    public function removeItem(int $itemId, ?int $variantId) {
        unset($this->content[$itemId][$variantId]);
    }
    
    public function sum() {
        $sum = 0;
        foreach ($this->content as $itemArray) {
            /** @var CartItem[] $itemArray */
            foreach ($itemArray as $item) {
                $sum += $item->Price * $item->Quantity;
            }
        }
        return $sum;
    }
    
    public function serialize() {
        $cart = [];
        foreach ($this->content as $itemArray) {
            /** @var CartItem[] $itemArray */
            foreach ($itemArray as $item) {
                $cart[$item->Id][$item->VariantId] = $item->serialize();
            }
        }
        return $cart;
    }
    
    public static function deserialize(array $cartArray) {
        $cart = new Cart();
        foreach ($cartArray as $cartItemArrayArray) {
            foreach ($cartItemArrayArray as $cartItemArray) {
                $cart->addItem(CartItem::deserialize($cartItemArray));
            }
        }
        return $cart;
    }
    
    public function clear() {
        $this->content = [];
    }
    
    public function isEmpty() {
        return count($this->content) == 0;
    }
}
