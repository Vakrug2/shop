<?php

namespace App\Domain\Cart;

use App\Domain\Model;

/**
 * @property int $Id
 * @property int $Count
 * @property int $VariantId
 */
class AddToCartForm extends Model {
    
    protected $fillable = [
        'Id',
        'Count',
        'VariantId'
    ];
    
    protected $rules = [
        'Id' => 'required',
        'Count' => 'required|integer|min:1',
    ];
    
    protected $validationMessages = [
        'Id.required' => 'Product Id not provided',
        'Count.required' => 'Product quantity not provided',
        'Count.integer' => 'Product quantity is not integer',
        'Count.min' => 'Product quantity must be greater than 0'
    ];
}
