<?php

namespace App\Domain\Cart;

use App\Domain\Product;
use App\Domain\ProductVariant;

class CartItem {
    
    public $Id;
    
    public $Name;
    
    public $Price;
    
    public $Quantity;
    
    public $VariantId;
    
    public $VariantText;
    
    public function serialize() {
        return [
            'Id' => $this->Id,
            'Name' => $this->Name,
            'Price' => $this->Price,
            'Quantity' => $this->Quantity,
            'VariantId' => $this->VariantId,
            'VariantText' => $this->VariantText
        ];
    }
    
    public static function deserialize(array $cartItemArray) {
        $cartItem = new CartItem();
        $cartItem->Id = $cartItemArray['Id'];
        $cartItem->Name = $cartItemArray['Name'];
        $cartItem->Price = $cartItemArray['Price'];
        $cartItem->Quantity = $cartItemArray['Quantity'];
        $cartItem->VariantId = $cartItemArray['VariantId'];
        $cartItem->VariantText = $cartItemArray['VariantText'];
        return $cartItem;
    }
    
    public static function createFromProduct(Product $product, int $qty, ?int $variantId) {
        $cartItem = new CartItem();
        $cartItem->Id = $product->Id;
        $cartItem->Name = $product->Name;
        if ($variantId) {
            /** @var ProductVariant $productVariant */
            $productVariant = $product->findVariantById($variantId);
            $cartItem->Price = $productVariant->Price;
            $cartItem->VariantText = $productVariant->getVariantText();
        } else {
            $cartItem->Price = $product->Price;
            $cartItem->VariantText = "";
        }
        $cartItem->Quantity = $qty;
        $cartItem->VariantId = $variantId;
        return $cartItem;
    }
}
