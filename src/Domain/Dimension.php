<?php

namespace App\Domain;

/**
 * @property int $Id
 * @property string $Name
 * 
 * @property DimensionValue[] $dimensionValues
 * @property ProductDimension[] $productDimensions
 */
class Dimension extends Model {
    protected $table = 'dimension';
    
    protected $fillable = [
        'Name'
    ];
    
    protected $rules = [
        'Name' => 'required'
    ];
    
    public function dimensionValues() {
        return $this->hasMany(DimensionValue::class, 'DimensionId', 'Id');
    }
    
    public function productDimensions() {
        return $this->hasMany(ProductDimension::class, 'DimensionId', 'Id');
    }
}
