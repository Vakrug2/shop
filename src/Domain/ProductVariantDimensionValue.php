<?php

namespace App\Domain;

/**
 * @property int $ProductVariantId
 * @property int $ProductId
 * @property int $DimensionValueId
 * 
 * @property ProductVariant $productVariant
 * @property Product $product
 * @property DimensionValue $dimensionValue
 */
class ProductVariantDimensionValue extends Model {
    protected $table = 'product_variant_dimension_value';
    
    protected $fillable = [
        'ProductVariantId', 'ProductId', 'DimensionValueId'
    ];
    
    protected $rules = [
        'ProductVariantId' => 'required',
        'ProductId' => 'required',
        'DimensionValueId' => 'required'
    ];
    
    public function productVariant() {
        return $this->belongsTo(ProductVariant::class, 'ProductVariantId', 'Id');
    }
    
    public function product() {
        return $this->belongsTo(Product::class, 'ProductId', 'Id');
    }
    
    public function dimensionValue() {
        return $this->belongsTo(DimensionValue::class, 'DimensionValueId', 'Id');
    }
}