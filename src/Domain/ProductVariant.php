<?php

namespace App\Domain;

/**
 * @property int $Id
 * @property float $Price
 * @property string $Image
 * 
 * @property ProductVariantDimensionValue[] $productVariantDimensionValues
 */
class ProductVariant extends Model {
    protected $table = 'product_variant';
    
    protected $fillable = [
        'Price'
    ];
    
    protected $rules = [
        'Price' => 'required|numeric|min:0'
    ];
    
    protected $validationMessages = [
        'Price.required' => 'Price is required',
        'Price.numeric' => 'Price is not numeric',
        'Price.min' => 'Price is less than :min'
    ];
    
    public function productVariantDimensionValues() {
        return $this->hasMany(ProductVariantDimensionValue::class, 'ProductVariantId', 'Id');
    }
    
    public function getVariantText() {
        if (count($this->productVariantDimensionValues) == 0) {
            return "";
        }
        
        $textArray = [];
        
        foreach ($this->productVariantDimensionValues as $productVariantDimensionValue) {
            $dimensionValue = $productVariantDimensionValue->dimensionValue;
            $textArray[$dimensionValue->DimensionId] = $dimensionValue->Value;
        }
        
        return "{" . implode(', ', $textArray) . "}";
    }
}