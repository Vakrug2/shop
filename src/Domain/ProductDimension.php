<?php

namespace App\Domain;

/**
 * @property int $ProductId
 * @property int $DimensionId
 * 
 * @property Product $product
 * @property Dimension $dimension
 */
class ProductDimension extends Model {
    protected $table = 'product_dimension';
    
    protected $fillable = [
        'ProductId', 'DimensionId'
    ];
    
    protected $rules = [
        'ProductId' => 'required',
        'DimensionId' => 'required'
    ];
    
    public function product() {
        return $this->belongsTo(Product::class, 'ProductId', 'Id');
    }
    
    public function dimension() {
        return $this->belongsTo(Dimension::class, 'DimensionId', 'Id');
    }
}