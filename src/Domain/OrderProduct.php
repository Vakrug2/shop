<?php

namespace App\Domain;

/**
 * @property int $Id
 * @property int $OrderId
 * @property int $ProductId
 * @property int $ProductVariantId
 * @property float $Price
 * @property int $Qty
 * 
 * @property Product $product
 * @property Order $order
 * @property ProductVariant $productVariant
 */
class OrderProduct extends Model {
    protected $table = 'order_product';
    
    protected $fillable = [
        'OrderId', 'ProductId', 'ProductVariantId', 'Price', 'Qty'
    ];
    
    public function order() {
        return $this->belongsTo(Order::class, 'OrderId', 'Id');
    }
    
    public function product() {
        return $this->belongsTo(Product::class, 'ProductId', 'Id');
    }
    
    public function productVariant() {
        return $this->belongsTo(ProductVariant::class, 'ProductVariantId', 'Id');
    }
}
