<?php

namespace App\Domain;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\DatabasePresenceVerifier;
use Illuminate\Validation\Validator;
use Psr\Container\ContainerInterface;

class Model extends EloquentModel {
    /** @var ContainerInterface */
    protected $container;
    
    protected $rules = [];
    protected $validationMessages = [];
    /** @var MessageBag */
    public $errors;
    
    /** @var Validator */
    private $validator;
    
    protected $additionalFields = [];
    public $timestamps = false;
    protected $primaryKey = 'Id';
    
    public function __construct(array $attributes = []) {
        global $container;
        parent::__construct($attributes);
        $this->container = $container;
        $this->errors = new MessageBag();
    }
    
    public function validate() {
        if ($this->getValidator()->fails()) {
            $this->errors = $this->getValidator()->errors();
            return false;
        }
        return true;
    }
    
    private function getValidator() {
        if (!$this->validator) {
            $this->validator = new Validator($this->container->get('Illuminate.Translation.Translator'), $this->getAttributes(), $this->rules, $this->validationMessages);
            $capsule = $this->container->get(Manager::class);
            $presenceVerifier = new DatabasePresenceVerifier($capsule->getDatabaseManager());
            $this->validator->setPresenceVerifier($presenceVerifier);
        }
        
        return $this->validator;
    }
    
    public function save(array $options = []) {
        //remove additional fields
        foreach ($this->additionalFields as $additionalFieled) {
            unset($this->attributes[$additionalFieled]);
        }
        
        return parent::save($options);
    }
}
