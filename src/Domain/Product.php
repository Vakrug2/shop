<?php

namespace App\Domain;

use Exception;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * @property int $Id
 * @property string $Name
 * @property float $Price
 * 
 * @property OrderProduct[] $orderProducts
 * @property ProductDimension[] $productDimensions
 * @property ProductVariantDimensionValue[] $productVariantDimensionValues
 */
class Product extends Model {
    protected $table = 'product';
    
    protected $fillable = [
        'Name', 'Price'
    ];
    
    protected $rules = [
        'Name' => 'required',
        'Price' => 'required|numeric|min:0'
    ];
    protected $validationMessages = [
        'Name.required' => 'Name is required',
        'Price.required' => 'Price is required',
        'Price.numeric' => 'Price is not numeric',
        'Price.min' => 'Price is less than :min'
    ];
    
    protected $attributes = [
        'Name' => '',
        'Price' => 0
    ];
    
    public function orderProducts() {
        return $this->hasMany(OrderProduct::class, 'ProductId', 'Id');
    }
    
    public function productDimensions() {
        return $this->hasMany(ProductDimension::class, 'ProductId', 'Id');
    }
    
    public function productVariantDimensionValues() {
        return $this->hasMany(ProductVariantDimensionValue::class, 'ProductId', 'Id');
    }
    
    public function checkVariantIdIsLegal(int $variantId) {
        $isLegal = false;
        foreach ($this->productVariantDimensionValues as $productVariantDimensionValue) {
            if ($productVariantDimensionValue->ProductVariantId == $variantId) {
                $isLegal = true;
                break;
            }
        }
        return $isLegal;
    }
    
    public function findVariantById(int $variantId) {
        $found = false;
        $variant = null;
        
        foreach ($this->productVariantDimensionValues as $productVariantDimensionValue) {
            if ($productVariantDimensionValue->ProductVariantId == $variantId) {
                $found = true;
                $variant = $productVariantDimensionValue->productVariant;
                break;
            }
        }
        
        if (!$found) {
            throw new Exception("Product variant not found. (ProductId = $this->Id, VariantId = $variantId)");
        }
        return $variant;
    }
    
    public function dimensionsText() {
        $textArr = [];
        foreach ($this->productDimensions as $productDimension) {
            $textArr[] = $productDimension->dimension->Name;
        }
        if (empty($textArr)) {
            return "";
        }
        return '{' . implode(', ', $textArr) . '}';
    }
    
    /**
     * @param Dimension[] $dimensions
     */
    public function createdProductVariants($dimensions) {
         try {
            DB::beginTransaction();
            foreach ($dimensions as $dimension) {
                $productDimenstion = new ProductDimension();
                $productDimenstion->ProductId = $this->Id;
                $productDimenstion->DimensionId = $dimension->Id;
                $productDimenstion->save();
            }

            //cartesian product https://stackoverflow.com/questions/6311779/finding-cartesian-product-with-php-associative-arrays
            $result = [[]];
            foreach ($dimensions as $key => $dimension) {
                $append = [];
                foreach ($dimension->dimensionValues as $dimensionValue) {
                    foreach ($result as $data) {
                        $append[] = $data + [$key => $dimensionValue];
                    }
                }
                $result = $append;
            }

            foreach ($result as $values) {
                /** @var DimensionValue[] $values */
                $productVariant = new ProductVariant();
                $productVariant->Price = $this->Price;
                $productVariant->save();
                foreach ($values as $value) {
                    $productVariantDimensionValue = new ProductVariantDimensionValue();
                    $productVariantDimensionValue->ProductVariantId = $productVariant->Id;
                    $productVariantDimensionValue->ProductId = $this->Id;
                    $productVariantDimensionValue->DimensionValueId = $value->Id;
                    $productVariantDimensionValue->save();
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
    
    public function deleteAllVariants() {
        try {
            DB::beginTransaction();
            
            foreach ($this->productVariantDimensionValues as $productVariantDimensionValue) {
                $productVariantDimensionValue->productVariant->delete();
            }
            
            ProductDimension::query()->where('ProductId', $this->Id)->delete();
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
