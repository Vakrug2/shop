-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.17 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for shop
CREATE DATABASE IF NOT EXISTS `shop` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `shop`;

-- Dumping structure for table shop.dimension
CREATE TABLE IF NOT EXISTS `dimension` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.dimension_value
CREATE TABLE IF NOT EXISTS `dimension_value` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DimensionId` int(11) NOT NULL,
  `Value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_dimension_value_dimension` (`DimensionId`),
  CONSTRAINT `FK_dimension_value_dimension` FOREIGN KEY (`DimensionId`) REFERENCES `dimension` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.order
CREATE TABLE IF NOT EXISTS `order` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `DateTime` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_order_user` (`UserId`),
  CONSTRAINT `FK_order_user` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.order_product
CREATE TABLE IF NOT EXISTS `order_product` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) NOT NULL,
  `ProductVariantId` int(11) DEFAULT NULL,
  `OrderId` int(11) NOT NULL,
  `Price` decimal(20,2) NOT NULL,
  `Qty` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `ProductId_OrderId` (`ProductId`,`OrderId`,`ProductVariantId`) USING BTREE,
  KEY `FK_order_items_order` (`OrderId`),
  KEY `FK_order_product_product_variant` (`ProductVariantId`),
  CONSTRAINT `FK_order_items_order` FOREIGN KEY (`OrderId`) REFERENCES `order` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_order_product` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`),
  CONSTRAINT `FK_order_product_product_variant` FOREIGN KEY (`ProductVariantId`) REFERENCES `product_variant` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.product
CREATE TABLE IF NOT EXISTS `product` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Price` decimal(20,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.product_dimension
CREATE TABLE IF NOT EXISTS `product_dimension` (
  `ProductId` int(11) NOT NULL,
  `DimensionId` int(11) NOT NULL,
  UNIQUE KEY `ProductId_DimensionId` (`ProductId`,`DimensionId`),
  KEY `FK__dimension` (`DimensionId`),
  CONSTRAINT `FK__dimension` FOREIGN KEY (`DimensionId`) REFERENCES `dimension` (`Id`),
  CONSTRAINT `FK__product` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.product_variant
CREATE TABLE IF NOT EXISTS `product_variant` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `Image` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.product_variant_dimension_value
CREATE TABLE IF NOT EXISTS `product_variant_dimension_value` (
  `ProductVariantId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `DimensionValueId` int(11) NOT NULL,
  UNIQUE KEY `ProductVariantId_ProductId_DimensionValueId` (`ProductVariantId`,`ProductId`,`DimensionValueId`),
  KEY `FK__product_variant_dimension_value_product` (`ProductId`),
  KEY `FK__dimension_value` (`DimensionValueId`),
  CONSTRAINT `FK__dimension_value` FOREIGN KEY (`DimensionValueId`) REFERENCES `dimension_value` (`Id`),
  CONSTRAINT `FK__product_variant` FOREIGN KEY (`ProductVariantId`) REFERENCES `product_variant` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK__product_variant_dimension_value_product` FOREIGN KEY (`ProductId`) REFERENCES `product` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table shop.user
CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IsActive` int(11) NOT NULL DEFAULT '0',
  `IsAdmin` int(11) NOT NULL DEFAULT '0',
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PasswordHash` text COLLATE utf8_unicode_ci,
  `ActivationHash` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
