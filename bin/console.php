<?php

use App\Application\Settings\SettingsInterface;
use DI\Container;
use DI\ContainerBuilder;
use Illuminate\Database\Capsule\Manager;
use Symfony\Component\Console\Application as ConsoleApplication;

require_once __DIR__ . '/../vendor/autoload.php';

// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

if (false) { // Should be set to true in production
    $containerBuilder->enableCompilation(__DIR__ . '/../var/cache');
}

// Set up settings
$settings = require __DIR__ . '/../app/settings.php';
$settings($containerBuilder);

// Set up dependencies
$dependencies = require __DIR__ . '/../app/dependencies.php';
$dependencies($containerBuilder);

// Build PHP-DI Container instance
/** @var Container $container */
$container = $containerBuilder->build();

/** @var SettingsInterface $settings */
$settings = $container->get(SettingsInterface::class);

//Activate eloquent
$capsule = new Manager();
$capsule->addConnection($settings->get('db'));
$capsule->setAsGlobal(); //In order to use DB::
$capsule->bootEloquent();
$container->set(Manager::class, $capsule);

$application = $container->get(ConsoleApplication::class);
$application->run();